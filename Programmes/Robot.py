from tkinter import Tk, Canvas, Toplevel, Entry, Button
from time import time, sleep
from math import sqrt, cos, sin, acos, asin, pi
from random import random, randint
from Tkconvert import *

################################ Constantes ####################################

ALL = "all"
END = "end"

################################ Classes #######################################

class Robot(object):
    """
    Cette classe s'occupe uniquement de l'initialisation du robot incrémentiel.
    À ce stade-ci, il est encore possible, qu'il y aie des fonctions qui devrait
    se trouver dans une classe fille.
    
    Il s'agit de la version avec visualisation.
    """
    def __init__(self):
        """
        Initialisation des variables importants:
            Master: Fenêtre mère
            Canvas: Canvas normal, mais avec l'origin au centre
            
            Rayon: rayon d'un bras de robot
            Position: il s'agit de la position de l'articulation et de 
                      l'extrémité pour un rayon imaginaire de 1
            VPosition: il s'agit de la position qu'aura ces points sur le 
                       graphique (multiplication pas Rayon)
            PosAngles: angles correspondants à Position/VPosition
            
            Increment: définie de combien d'angle un bras bouge par incrément
            AngleChangement: tous les mouvements possible du bras
            
            Distance: distance entre le point actuel et le point finale
            NewDistance: distance à comparer avec Distance
            
            TimeStep: pour compter le nombre de mouvement: (incrément par mouvement, précision)
            
            Window: fenêtre fille qui s'occupe de la lecture des coordonnées
            EnterX/EnterY/Confirm: <form></form>
        """
        self.Master = Tk()
        self.Master.geometry("500x500+100+100")
        self.Master.title("Robot simulation (\"path\")")
        
        self.Canvas = Canvas2(self.Master, height=500, width=500)
        self.Canvas.grid()
        
        self.Rayon = 100
        self.Position = ((1, 0), (2, 0))
        self.VPosition = ((self.Rayon, 0), (2*self.Rayon, 0))
        self.PosAngles = [0, 0]
        
        self.Increment = pi/360
        self.AngleChangements = [
                                 (self.Increment, self.Increment),
                                 (-self.Increment, -self.Increment),
                                 (-self.Increment, self.Increment),
                                 (self.Increment, -self.Increment),
                                 (0, self.Increment),
                                 (self.Increment, 0),
                                 (0, -self.Increment),
                                 (-self.Increment, 0)
                                ]
        
        
        self.Distance = -1
        self.NewDistance = -1
        
        step = 0.001
        self.TimeStep = (step, len(str(step)))
        
        self.Window = Toplevel(self.Master, bg="white")
        self.Window.geometry("200x100+900+300")
        self.Window.title("Coordinates")
        
        self.EnterX = Entry(self.Window)
        self.EnterX.pack()
        self.EnterY = Entry(self.Window)
        self.EnterY.pack()
        self.Confirm = Button(self.Window, text="Enter", command=self.read)
        self.Confirm.pack()
        
        
    def read(self):
        """
        Cette fonction s'occupe de lire les coordonnées entrées dans la 
        fenêtre fille "Window"
        """
        try:
            x = float(self.EnterX.get())
            y = float(self.EnterY.get())
            
            self.EnterX.delete(0, END)
            self.EnterY.delete(0, END)
            
            
            self.setTarget(x, y)        # Les coordonnées sont passées à setTarget
                        
            return self.start()         # Le robot exécute l'ordre
        
        except ValueError:
            return False
            
    
    def setTarget(self, x, y):
        """
        Initialisation des variables concernant le point d'arrivée:
        
        Target: Coordonnées du point d'arrivée (valeur théorique, Rayon=1)
        VTarget: Coordonnées pour l'afficahge (multiplication par "Rayon")
        Diff: Distance sur les abscisses entre le point actuel et le point 
              d'arrivée
        AbsDiff: Diff en valeur absolue
        BigDiff: recherche la plus grande des deux distance
        """
        if not isinstance(x, (float, int)):
            raise ValueError
        if not isinstance(y, (float, int)):
            raise ValueError
        
        self.Target = (x, y)
        self.VTarget = (x*self.Rayon, y*self.Rayon)
        self.Diff = [self.Position[1][0] - x, self.Position[1][1] - y]
        
        AbsDiff = [abs(self.Diff[0]), abs(self.Diff[1])]
        bigger = AbsDiff.index(max(AbsDiff))
        
        self.BigDiff = [self.Diff[bigger], bigger]
        print(self.BigDiff)
        
        return self.calcLine()          # Calcule ensuite la ligne parfaite entre Target et le point actuel
        
    def calcLine(self):
        """
        Line: contient les informations de la droite parfaite (coéfficients 
                                                               de l'équation
                                                               cartésienne)
        """
        A = self.Position[1][1] - self.Target[1]                # On calcule les coordonnées
        B = self.Target[0] - self.Position[1][0]                # du vecteur AB, puis, on
        C = - (A * self.Target[0] + B * self.Target[1])         # calcule C.
        
        self.Line = (A, B, C)
        return True
        
    
    def start(self):
        """
        Rassemble les étapes nécessaires pour calculer les mouvements et
        les faire afficher.
        """
        
        # On fait disparaître tout de l'écran
        self.Canvas.delete(ALL)
        
        # Il faudrait faire entrer dans Tkconvert pour ne pas avoir à changer l'origine ici
        self.Canvas.create_oval(250-2*self.Rayon, 250-2*self.Rayon, 250+2*self.Rayon, 250+2*self.Rayon)
        
        beginning = self.VPosition
        # Ligne parfaite
        red = self.Canvas.createLine(self.VTarget[0], self.VTarget[1], beginning[1][0], beginning[1][1], color="red")
        
        # représentations du bras du robot (composé de deux lignes)
        self.Arms = [self.Canvas.createLine(0, 0,
                                            self.VPosition[0][0], self.VPosition[0][1]),
                     self.Canvas.createLine(self.VPosition[0][0], self.VPosition[0][1],
                                            self.VPosition[1][0], self.VPosition[1][1])
                    ]
        
        # affichage du temps écoulé
        t = 0
        timetext = self.Canvas.create_text(50, 20, text=str(t))
        
        # boucle qui se termine quand on arrive au point d'arrivée
        # (jusqu'alors encore théorique, car ne marche pas dans la réalité)
        
        # La condition utilisée: pour commencer, Distance et NewDistance sont à -1 < 0,
        # la boucle débute et s'arrête quand Distance <= NewDistance (SI LE BRAS DÉPASSE LE POINT D'ARRIVÉE)
        while (self.Distance < 0 or self.NewDistance < 0 or self.Distance >= self.NewDistance):
            
            #print(self.Distance)        
            
            # on incrémente le temps
            t += self.TimeStep[0]
            
            # on fait un clear (on laisse le cercle et la ligne parfaite)
            self.Canvas.delete(self.Arms[0])
            self.Canvas.delete(self.Arms[1])
            
            
            # on calcule la position qui sera affichée à partir de Position
            self.VPosition = (
                              (self.Position[0][0]*self.Rayon, 
                               self.Position[0][1]*self.Rayon),
                              (self.Position[1][0]*self.Rayon,
                               self.Position[1][1]*self.Rayon)
                              )
            
            # on redessine les bras
            self.Arms = [self.Canvas.createLine(0, 0,
                                               self.VPosition[0][0], self.VPosition[0][1]),
                        self.Canvas.createLine(self.VPosition[0][0], self.VPosition[0][1],
                                               self.VPosition[1][0], self.VPosition[1][1])
                        ]
            
            # on fait afficher le temps
            self.Canvas.itemconfig(timetext, text="t="+str(t)[:self.TimeStep[1]])
            self.Canvas.update()
            self.Master.update()
            
            # La nouvelle distance devient la distance actuelle
            self.Distance = self.NewDistance
            # On fait calculer le prochain mouvement (turn n'existe que dans la classe fille, à amméliorer)
            self.turn()
            # On attend pour que l'utilisateur voit ce qui se passe
            sleep(self.TimeStep[0])
        
        # après l'arrivée, on remet tout à -1 pour pouvoir recommencer la boucle
        self.Distance = -1
        self.NewDistance = -1
        
        self.setTarget(-2*random()+1, -2*random()+1)
        self.start()
        
    
    def show(self):
        # pour faire afficher le robot.
        self.Master.mainloop()
        
class Robot_path(Robot):
    """
    Il s'agit d'une classe fille de Robot, qui se spécialise à une optimisation
    du CHEMIN.
    """
    def __init__(self):
        """
        Initialisation des informations issues de Robot
        """
        Robot.__init__(self)
    
    def turn(self):
        """
        Cette fonction calcule les mouvements à faire
        """
        distance = -1
        
        # boucle qui parcourt tous les mouvement possible pour en choisir le meilleur
        for change in self.AngleChangements:
            # newAngles contient les Angles qui définisseraient la nouvelle position
            newAngles = (self.PosAngles[0] + change[0], self.PosAngles[1] + change[1])
            
            # newPos contient les coordonnées de la nouvelle position
            newPos = (
                      (
                       cos(newAngles[0]),
                       sin(newAngles[0])
                      ),
                      (
                       cos(newAngles[0]+newAngles[1]) + cos(newAngles[0]),
                       sin(newAngles[0]+newAngles[1]) + sin(newAngles[0])
                      )
                     )
            
            # il s'agit de voire si le bras se déplacerait dans la bonne direction
            # (en ce moment, le test ne s'intéresse qu'à la distance x
            # il faut maintenant faire entrer le y. Possibilité: Choisir la distance
            # la plus grande.
            ordre = [self.Position[1][0] - newPos[1][0], self.Position[1][1] - newPos[1][1]]
            if not self.BigDiff[0] * ordre[self.BigDiff[1]] <= 0:
                
                # on calcule la nouvelle distance à la droite parfaite
                distance2 = (self.Line[0]*newPos[1][0] + self.Line[1]*newPos[1][1] + self.Line[2])**2
                
                # et teste si elle est meilleure que la vieille
                if (distance2 < distance or distance < 0):
                    # si oui, on remplace la vieille par la nouvelle
                    distance = distance2
                    myCoords = (newPos, newAngles)
        
        # Les coordonnées sont remplacées par les nouvelles coordonnées
        # et on calcule la nouvelle distance qui en résulte.
        self.Position = myCoords[0]
        self.PosAngles = myCoords[1]
        
        TargetDiffs = [self.Position[1][0] - self.Target[0], self.Position[1][1] - self.Target[1]]
        self.NewDistance = abs(TargetDiffs[self.BigDiff[1]])
        
        return True
    
################################



################################
r = Robot_path()
r.show()