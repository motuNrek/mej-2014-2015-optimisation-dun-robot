# Es handelt sich um ein Modul, dass die visuelle Darstellung vereinfachen soll
# Wenn man Canvas2, anstatt Canvas, benutzt, dann befindet sich das Origine in
# der Mitte des Canvas. Das ist aber nur für Linien gewährleistet. Ansonsten kann man die Funktion convert benutzten, um die entsprechenden Koordinaten zu erhalten.
#

from tkinter import Tk, Canvas

class Canvas2(Canvas):
    def __init__(self, master, width, height, bg="white"):
        self.Master = master
        self.Width = width
        self.Height = height
        self.BG = bg
        del master, width, height, bg
        
        Canvas.__init__(self, self.Master, width=self.Width, height=self.Height, bg=self.BG)
        
        self.O = (self.Width/2, self.Height/2)
        
    def convert(self, x1,y1,x2,y2):
        return x1+self.O[0], self.O[1]-y1, x2+self.O[0], self.O[1]-y2
    
    def createLine(self, x1, y1, x2, y2, status="normal", color="black"):
        x1, y1, x2, y2 = self.convert(x1, y1, x2, y2)
        return self.create_line(x1, y1, x2, y2, state=status, fill=color)
            
def main():
    root = Tk()
    root.geometry("700x700+200+100")
    
    canvas = Canvas2(root, 700, 700)
    canvas.grid()
    
    canvas.createLine(0, 0, 100, 0)
    canvas.update()
    
    root.mainloop()

if __name__ == "__main__":
    main()
